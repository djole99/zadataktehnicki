
let planetNameHeader = document.getElementById("TextPlanetNameHeader").firstElementChild;
let planetInfo = document.getElementById("WraperPlanetInfo");

createPlanetPage();
function createPlanetPage() {
    let queryString = window.location.search;
    let urlParams = new URLSearchParams(queryString);
    planetId = urlParams.get("id");
    fetch(`http://localhost:3001/api/planets/${planetId}`).then(function (response) {
        // The API call was successful!
        return response.json();
    }).then(function (data) {
        // This is the JSON from our response
        planet = data;
        fillHTML(data);
    }).catch(function (err) {
        // There was an error
        planetInfo.innerText = `Error: ${err.message}!`;
        console.warn('Something went wrong.', err);
    });

}
/*Kreiranje HTML-a sa svim informacijama o izabranoj tabeli*/
function fillHTML(planet) {
    planetNameHeader.innerText = planet.planetName;
    planetInfo.innerHTML = ` <div id="BasePlanetData">
 <div>
     <img src="${planet.imageUrl}" alt="${planet.imageName}"
         width="150" height="150" />
 </div>
 <div>
     <div>
         <span>${planet.planetName}</span>
     </div>
     <div>
         <span> ${planet.description}</span>
     </div>
 </div>
</div>
<div id="OtherPlanetData">
 <div id="FirstColum">
     <div>
         <span>
         ${planet.planetRadiusKM}
         </span>
         <span>
             Radius in km
         </span>
     </div>
     <div>
         <span>
         ${planet.planetColor}
         </span>
         <span>
             Color
         </span>
     </div>
 </div>
 <div id="SecondColum">
     <div>
         <span>
         ${planet.distInMillionsKM.fromSun}
         </span>
         <span>
            Dist. from Sun
         </span>
     </div>
     <div>
         <span>
         ${planet.distInMillionsKM.fromEarth}
         </span>
         <span>
             Dist. from Earth
         </span>
     </div>
 </div>
</div>`;
}

let editeWindow = document.getElementById("EditeWindow");
let confirmationModalWindow = document.getElementById("ConfirmationModalWindow");
let btnChoosefile = document.getElementById("BtnChoosefile");
let photoName = document.querySelector("#IPhoto>span");

document.getElementById("BtnEdit").addEventListener("click", () => {
    fillModalWindow(planet);
    document.getElementById("ModalWindow").removeAttribute("Class");
    editeWindow.removeAttribute("Class");
});

document.getElementById("BtnBrowse").addEventListener("click", () => {
    btnChoosefile.click();
});

document.getElementById("ModalWindow").addEventListener("click", (e) => {
    if (e.target.id === "ModalWindow" || e.target.classList.contains("BtnCanc") || e.target.id === "BtnX") {
        document.getElementById("ModalWindow").classList.add("Hide");
        editeWindow.classList.add("Hide");
        confirmationModalWindow.classList.add("Hide");
    }

});

/*Popunjavanje modalnog prozora sa svim informacijama o planeti*/
function fillModalWindow(planet) {
    btnChoosefile.value = "";
    photoName.innerText = "Photo"
    document.getElementById("InputName").value = planet.planetName;
    document.getElementById("InputDesc").value = planet.description;
    document.getElementById("InputRadius").value = planet.planetRadiusKM;
    document.getElementById("InputDistS").value = planet.distInMillionsKM.fromSun;
    document.getElementById("InputColor").value = planet.planetColor;
    document.getElementById("InputDistE").value = planet.distInMillionsKM.fromEarth;
}

/*Dodavanje EventListenera na btns Confirm. 
I u listeneru izvrsena provera na koje je dugme stisnuto i u zavisnosti od toga pokrenuti odredjenu akciju
*/
let wrapperBtns = document.getElementById("WrapperBtns");
let btnsConfirm = document.getElementsByClassName("BtnConfirm");
let titleConfirmWindow = document.querySelector("#ConfirmationModalWindow > span:first-of-type");
let textConfirmWindow = document.querySelector("#ConfirmationModalWindow > span:last-of-type");
/*let namePlanet = document.querySelector("#TextPlanetNameHeader > span");*/
for (const btn of btnsConfirm) {
    btn.addEventListener("click", () => {
        if (wrapperBtns.contains(btn)) {
            if (CheckInputs()) {
                editeWindow.classList.add("Hide");
                titleConfirmWindow.innerHTML = "Confirm editing";
                textConfirmWindow.innerHTML = `Are you sure you want to edit <b>${planet.planetName}</b>?`;
                confirmationModalWindow.removeAttribute("Class");
            }
        } else if (confirmationModalWindow.classList.contains("Delete")) {
            DeletePlanet(planetId);
        } else {
            EditPlanet();
        }
    });
}

document.getElementById("BtnDelete").addEventListener("click", () => {
    titleConfirmWindow.innerHTML = "Confirm deleting";
    textConfirmWindow.innerHTML = `Are you sure you want to delete <b>${planet.planetName}</b>?`;
    confirmationModalWindow.classList.add("Delete")
    confirmationModalWindow.classList.remove("Hide");
    document.getElementById("ModalWindow").removeAttribute("Class");
});

/*Brisanje panete i redirect na planets.html*/
function DeletePlanet(planetID) {
    fetch(`http://localhost:3001/api/planets/${planetID}`, {
        method: 'DELETE'
    })
        .then(res => res.json())
        .then(data => {
            // Do some stuff...
            window.location.href = "planets.html";
            console.log("success");
        })
        .catch(err => console.log(err));
}

btnChoosefile.addEventListener("change", () => {
    if (btnChoosefile.value) {
        photoName.innerText = btnChoosefile.files[0].name;
        const file = btnChoosefile.files[0];
        reader = new FileReader();
        reader.readAsDataURL(file);
    } else {
        photoName.innerText = "Photo"
    }
});

/*Izmena informacija o planeti i izvrsavanje reloada stranice kako bi promene odma bile vidljive.
Ukoliko nije popunjeno polje za sliku ostaje stara! 
*/
let planetName = document.getElementById("InputName");
let planetDesc = document.getElementById("InputDesc");
let planetRadius = document.getElementById("InputRadius");
let planetDistS = document.getElementById("InputDistS");
let planetColor = document.getElementById("InputColor");
let planetDistE = document.getElementById("InputDistE");

function EditPlanet() {
    //Obj of data to send in future like a dummyDb
    let urlSlike = planet.imageUrl;
    let imageName = planet.imageName;
    if (btnChoosefile.value) {
        urlSlike = reader.result;
        imageName = btnChoosefile.files[0].name;
    }
    const data = {
        "id": planetId,
        "planetName": planetName.value,
        "planetColor": planetColor.value,
        "planetRadiusKM": planetRadius.value,
        "distInMillionsKM": {
            "fromSun": planetDistS.value,
            "fromEarth": planetDistE.value
        },
        "description": planetDesc.value,
        "imageUrl": urlSlike,
        "imageName": imageName
    };

    //POST request with body equal on data in JSON format
    fetch(`http://localhost:3001/api/planets/${planetId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        //Then with the data from the response in JSON...
        .then((data) => {
            location.reload();
            console.log('Success:', data);
        })
        //Then with the error genereted...
        .catch((error) => {
            console.error('Error:', error);
        });
}

/*Provera inputa*/
function CheckInputs() {
    let indicator = false;
    planetName.classList.remove("Error");
    planetDesc.classList.remove("Error");
    planetColor.classList.remove("Error");
    planetRadius.classList.remove("Error");
    planetDistS.classList.remove("Error");
    planetDistE.classList.remove("Error");

    if (!planetName.value || !isNaN(planetName.value)) {
        indicator = true;
        planetName.classList.add("Error");
    }
    if (!planetDesc.value || !isNaN(planetDesc.value)) {
        indicator = true;
        planetDesc.classList.add("Error");
    }
    if (!planetColor.value || !isNaN(planetColor.value)) {
        indicator = true;
        planetColor.classList.add("Error");
    }
    if (!planetRadius.value || isNaN(planetRadius.value)) {
        indicator = true;
        planetRadius.classList.add("Error");
    }
    if (!planetDistS.value || isNaN(planetDistS.value)) {
        indicator = true;
        planetDistS.classList.add("Error");
    }
    if (!planetDistE.value || isNaN(planetDistE.value)) {
        indicator = true;
        planetDistE.classList.add("Error");
    }

    return indicator ? false : true;

}