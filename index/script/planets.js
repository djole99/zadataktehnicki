/*Kad se koristi back na browseru radim refresh da ocita nove podatke u slucaju izvrsenog edita */
if (performance.navigation.type == 2) {
	location.reload();
}

fetch('http://localhost:3001/api/planets').then(function (response) {
	// The API call was successful!
	return response.json();
}).then(function (data) {
	// This is the JSON from our response
	temporaryPlanets = data;
	FIllAllPlanetsCars(data);
	FillAllPlanetsTable(data);
	//console.log(data);
}).catch(function (err) {
	// There was an error
	document.getElementById("Results").innerText = `Error: ${err.message}!`;
	console.warn('Something went wrong.', err);
});

/*Dodavanje EventListenera na Card i Table image kako bih iskljucivao i ukljucivao zeljeni prikaz*/
let componentsHeaderImgs = document.querySelectorAll("#ComponentsHeader > img");
for (const img of componentsHeaderImgs) {
	img.addEventListener("click", (e) => {
		for (const img of componentsHeaderImgs) {
			img.classList.remove("Checked");
		}
		e.target.classList.add("Checked");
		if (e.target.getAttribute("alt") === "grid view") {
			document.getElementById("WrapperGridView").classList.remove("Hide");
			document.getElementById("WrapperTabelView").classList.add("Hide");
		} else {
			document.getElementById("WrapperGridView").classList.add("Hide");
			document.getElementById("WrapperTabelView").classList.remove("Hide");
		}
	});
}

/*Kreiranje kartica sa informacijama o planetama*/
function FIllAllPlanetsCars(planets) {
	let wrapperGridViewHTML = document.getElementById("WrapperGridView");
	wrapperGridViewHTML.innerHTML = "";
	for (const planet of planets) {
		let newEl = document.createElement("div");
		newEl.classList.add("Card");
		newEl.innerHTML = `<div class="WrapperBasicData">
		<div>
			<img src="${planet.imageUrl}" alt="${planet.imageName}"
				width="70" height="70" />
		</div>
		<div>
			<span>${planet.planetName}</span>
		</div>
		<div>
			<span>
			${planet.description}
			</span>
		</div>

	</div>
	<div class="WrapperOtherData">
		<div class="FirstColum">
			<div>
				<div>
					<span>${planet.planetColor}</span>
				</div>
				<div>
					<span>Color</span>
				</div>
			</div>
			<div>
				<div>
					<span>${planet.planetRadiusKM}</span>
				</div>
				<div>
					<span>Radius in km</span>
				</div>
			</div>
		</div>
		<div class="SecondColum">
			<div>
				<div>
					<span>${planet.distInMillionsKM.fromSun}</span>
				</div>
				<div>
					<span>Dist. from sun</span>
				</div>
			</div>
			<div>
				<div>
					<span>${planet.distInMillionsKM.fromEarth}</span>
				</div>
				<div>
					<span>Dist. from earth</span>
				</div>
			</div>
		</div>
	</div>`;
		wrapperGridViewHTML.append(newEl);
		newEl.addEventListener("click", () => {
			window.location.href = "planet.html" + `?&id=${planet.id}`;
		});
	}
}

/*Kreiranje tabelarnog prikaza sa informacijama o planetama.
 Dodavanje EventListenera na Name celiju u zaglavju kako bih mogao da sortiram opadajuce
 */
function FillAllPlanetsTable(planets) {
	let wrapperTabelViewHTML = document.getElementById("WrapperTabelView");
	wrapperTabelViewHTML.innerHTML = `<table style="width:100%">
	<tr>
		<th>Name<img src="resource/icons8-arrow-50.png" alt="" width="13" height="13"/></th>
		<th>Color</th>
		<th>Radius in km</th>
		<th>Dist from Sun</th>
		<th>Dist from Earth</th>
	</tr>
	</table>`;

	for (const planet of planets) {
		let newEl = document.createElement("tr");
		newEl.innerHTML = `<td class="WrapperBasicDataTableV">
	<div>
		<img src="${planet.imageUrl}"
		alt="${planet.imageName}" width="55" height="55" />
	</div>
	<div>
		<div><span>${planet.planetName}</span></div>
		<div><span>${planet.description}</span></div>
	</div>
</td>
<td>${planet.planetColor}</td>
<td>${planet.planetRadiusKM}</td>
<td>${planet.distInMillionsKM.fromSun}</td>
<td>${planet.distInMillionsKM.fromEarth}</td>`;
		newEl.addEventListener("click", () => {
			window.location.href = "planet.html" + `?&id=${planet.id}`;
		});
		wrapperTabelViewHTML.lastElementChild.lastElementChild.appendChild(newEl);

	}

	/*Dodavanje event listenera za sort po imenu planete*/
	let thName = document.querySelector("tr>th:first-of-type");
	AddSortEventByPlanetName(thName, planets);
}

/*U slucaju neparnih klikova sortira rastuce, a u slucaju parnih vraca kako je bilo pre sortiranja*/
/*PlanetsTemp promenljivu deklarisem kako bih imao podatke o planetama (originalnog redosleda) u koliko je izvrsena pretraga po inputu*/
let click = 0;
let planetsTemp;
function AddSortEventByPlanetName(th, planets) {
	th.addEventListener("click", () => {
		click++;
		if (click % 2 === 0) {
			if (planetsTemp) {
				FillAllPlanetsTable(planetsTemp);
			} else {
				FillAllPlanetsTable(temporaryPlanets);
			}
		} else {
			let temporary = planets.slice().sort(function (a, b) {
				let nameA = a.planetName.toUpperCase(); 
				let nameB = b.planetName.toUpperCase(); 
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
				return 0;

			});
			FillAllPlanetsTable(temporary);
		}
	});
}

let btnChoosefile = document.getElementById("BtnChoosefile");
let photoName = document.querySelector("#IPhoto>span");

document.getElementById("BtnCreate").addEventListener("click", () => {
	document.getElementById("ModalWindow").removeAttribute("Class")
});

let btnBrowse = document.getElementById("BtnBrowse");
btnBrowse.addEventListener("click", () => {
	btnChoosefile.click();
});

btnChoosefile.addEventListener("change", () => {
	if (btnChoosefile.value) {
		photoName.innerText = btnChoosefile.files[0].name;
		const file = btnChoosefile.files[0];
		reader = new FileReader();
		reader.readAsDataURL(file);
	} else {
		photoName.innerText = "Photo"
	}
});

document.getElementById("ModalWindow").addEventListener("click", (e) => {
	if (e.target.id === "ModalWindow" || e.target.id === "BtnCanc" || e.target.id === "BtnX") {
		document.getElementById("ModalWindow").classList.add("Hide");
	}

});

/*Kada filtriram na osnovu unosa iz inputa opet pozivam popunjavanje kartica i tabele sa onim planetama koji zadovoljavaju uslov*/
/*U planetsTemp cuvam podatke da bi mi bili dostupni u originalnom redosledu kada je broj klikova paran na sortu*/
let inputSearch = document.getElementById("InputSearch");
inputSearch.addEventListener("keyup", (e) => {
	if (e.key === "Enter") {
		temporary = temporaryPlanets.filter((planet) => {
			return planet.planetName.toUpperCase().includes(inputSearch.value.toUpperCase());
		});
		planetsTemp = temporary;
		FIllAllPlanetsCars(temporary);
		FillAllPlanetsTable(temporary);
	}
});

document.getElementById("BtnCre").addEventListener("click", () => {
	CreatePlanet();
});

let planetName = document.getElementById("InputName");
let planetDesc = document.getElementById("InputDesc");
let planetRadius = document.getElementById("InputRadius");
let planetDistS = document.getElementById("InputDistS");
let planetColor = document.getElementById("InputColor");
let planetDistE = document.getElementById("InputDistE");

/*Kreiranje planete nakon provere inputa i izvrsavanje reload-a stranice kako bi se odmah ocitala kreirana planeta*/
/*==========!!!!Problem sa upload-om slike!!!!==========*/
/*Link slike uzimam iz file readera (Radi samo do odredjene veličine slike vrv zbog ograničene veličine objekta)*/
function CreatePlanet() {
	if (CheckInputs()) {
		//Obj of data to send in future like a dummyDb
		const data =
		{
			"planetName": planetName.value,
			"planetColor": planetColor.value,
			"planetRadiusKM": planetRadius.value,
			"distInMillionsKM": {
				"fromSun": planetDistS.value,
				"fromEarth": planetDistE.value
			},
			"description": planetDesc.value,
			"imageUrl": reader.result,
			"imageName": btnChoosefile.files[0].name
		};

		//POST request with body equal on data in JSON format
		fetch(`http://localhost:3001/api/planets/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		})
			.then((response) => response.json())
			//Then with the data from the response in JSON...
			.then((data) => {
				location.reload();
				console.log('Success:', data);
			})
			//Then with the error genereted...
			.catch((error) => {
				console.error('Error:', error);
			});

	}
}

/*Provera inputa*/
function CheckInputs() {
	let indicator = false;
	planetName.classList.remove("Error");
	planetDesc.classList.remove("Error");
	planetColor.classList.remove("Error");
	planetRadius.classList.remove("Error");
	planetDistS.classList.remove("Error");
	planetDistE.classList.remove("Error");
	btnBrowse.classList.remove("Error");


	if (!planetName.value || !isNaN(planetName.value)) {
		indicator = true;
		planetName.classList.add("Error");
	}
	if (!planetDesc.value || !isNaN(planetDesc.value)) {
		indicator = true;
		planetDesc.classList.add("Error");
	}
	if (!planetColor.value || !isNaN(planetColor.value)) {
		indicator = true;
		planetColor.classList.add("Error");
	}
	if (!planetRadius.value || isNaN(planetRadius.value)) {
		indicator = true;
		planetRadius.classList.add("Error");
	}
	if (!planetDistS.value || isNaN(planetDistS.value)) {
		indicator = true;
		planetDistS.classList.add("Error");
	}
	if (!planetDistE.value || isNaN(planetDistE.value)) {
		indicator = true;
		planetDistE.classList.add("Error");
	}
	if (!btnChoosefile.value) {
		indicator = true;
		btnBrowse.classList.add("Error");
	}

	return indicator ? false : true;

}
